package org.mpierce.gradle.jmh;

import org.openjdk.jmh.annotations.GenerateMicroBenchmark;

public class SillyBenchmark {

    @GenerateMicroBenchmark
    public double pow() {
        return Math.pow(42, 17);
    }
}
